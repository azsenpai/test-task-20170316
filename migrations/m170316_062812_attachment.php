<?php

use yii\db\Migration;

/**
 *
 */
class m170316_062812_attachment extends Migration
{
    const TABLE_NAME = 'attachment';

    /**
     *
     */
    public function up()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id' => $this->primaryKey(),
                'document_id' => $this->integer()->notNull(),
                'name' => $this->string()->notNull(),
                'filename' => $this->string()->notNull(),
                'size' => $this->integer()->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ]
        );

        $this->createIndex(
            sprintf('idx-%s-document_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'document_id'
        );

        $this->addForeignKey(
            sprintf('fk-%s-document_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'document_id',
            'document',
            'id'
        );
    }

    /**
     *
     */
    public function down()
    {
        $this->dropForeignKey(
            sprintf('fk-%s-document_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropIndex(
            sprintf('idx-%s-document_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        $this->dropTable(self::TABLE_NAME);
    }
}
