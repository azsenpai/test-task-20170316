<?php

use yii\db\Migration;

/**
 *
 */
class m170316_062804_document extends Migration
{
    const TABLE_NAME = 'document';

    /**
     *
     */
    public function up()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'description' => $this->text()->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ]
        );
    }

    /**
     *
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
