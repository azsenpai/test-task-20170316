<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Document */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(
    '@web/js/document-form.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>

<div class="document-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <label class="control-label">Attachments:</label>

    <?php foreach ($model->attachments as $attachment): ?>
        <div class="form-group">
            <?= sprintf('%s (%s)', $attachment->name, $attachment->size) ?>
            <?= Html::a('Delete', '#', [
                'class' => 'delete-attachment',
                'data' => [
                    'id' => $attachment->id,
                ],
            ]) ?>
        </div>
    <?php endforeach ?>

    <div class="upload-file-html hidden">
        <?= $form->field($uploadForm, 'files[]', [
            'template' => "
                {label}\n
                <div class=\"clearfix\">\n
                    <span class=\"pull-left\" style=\"margin:5px 5px 0 0;\">{input}</span>\n
                    <span class=\"pull-left btn-remove-attachment btn btn-sm btn-danger glyphicon glyphicon-remove\"></span>\n
                </div>\n
                {hint}\n
                {error}
            ",
        ])->fileInput()->label(false) ?>
    </div>

    <div class="form-group">
        <span class="btn-add-attachment btn btn-sm btn-primary">Add attachment</span>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>