<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    public $document_id;

    /**
     * @var UploadedFile[]
     */
    public $files;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['document_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'files' => 'Attachments',
        ];
    }

    /**
     *
     */
    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->files as $file) {
                $attachment = new Attachment([
                    'document_id' => $this->document_id,
                    'name' => $file->baseName,
                    'filename' => uniqid() . '.' . $file->extension,
                    'size' => $file->size,
                ]);

                if ($attachment->save()) {
                    $file->saveAs('uploads/' . $attachment->filename);
                }
            }

            return true;
        }

        return false;
    }
}
