USING
-----
1. `git clone https://coder_iitu@bitbucket.org/coder_iitu/test-task-20170316.git`

2. `cd test-task-20170316`

3. `composer install`

4. `yii serve/index`

5. Check the address http://localhost:8080

DB schema
---------

![db-schema.png](https://bitbucket.org/repo/MrgXMBd/images/3390056832-db-schema.png)