jQuery(function($) {

    var uploadFileHtml = $('.upload-file-html').html();

    $('.btn-add-attachment').on('click', function() {
        $(this).parent().before(uploadFileHtml);
    });

    $('form').on('click', '.btn-remove-attachment', function() {
        var $formGroup = $(this).parents('.form-group');
        $formGroup.remove();
    });

    $('.delete-attachment').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        var id = $this.data('id');

        $.ajax({
            type: 'post',
            url: '/document/delete-attachment',
            data: {
                id: id
            },
            success: function(response) {
                if ('status' in response) {
                    if (response.status == 'ok') {
                        $this.parents('.form-group').remove();
                    }
                }
            }
        });
    });

});